#include "renderingarea.h"
#include <QPainter>
#include <QTimer>

RenderingArea::RenderingArea(QWidget *parent): QOpenGLWidget(parent)
{
    testRectangle = new MovingRectangle;
    painter = new QPainter;
    timer =  new QTimer;
    connect(timer,SIGNAL(timeout()),this,SLOT(update()));
    timer->start(25);
}
RenderingArea::~RenderingArea(){

    delete timer;
    delete painter;
    delete testRectangle;
}
void RenderingArea::initializeGL()
{
    QOpenGLWidget::initializeGL();
}

void RenderingArea::resizeGL(int w, int h)
{
    QOpenGLWidget::resizeGL(w, h);
}

void RenderingArea::paintGL()
{
   painter->begin(this);

   testRectangle->step(); // update game
   testRectangle->draw(this->painter); // draw on screen !

   painter->end();
}
