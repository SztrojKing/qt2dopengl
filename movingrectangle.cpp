#include "movingrectangle.h"

MovingRectangle::MovingRectangle():
    increment(0) // <- meme chose que mettre increment = 0 dans la fonction
{

}

// update game
void MovingRectangle::step()
{
    increment++;
}

// draw game on painter
void MovingRectangle::draw(QPainter *painter)
{
    painter->fillRect(10+2*(increment%200), 10+increment%200, 10, 10, Qt::red);
}
