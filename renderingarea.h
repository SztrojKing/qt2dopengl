#ifndef RENDERINGAREA_H
#define RENDERINGAREA_H
#include "movingrectangle.h"

#include <QOpenGLWidget>

class RenderingArea : public QOpenGLWidget
{
    Q_OBJECT
public:
    RenderingArea(QWidget *parent);
    ~RenderingArea();

    // QOpenGLWidget interface
protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();


private:
    QTimer *timer;
    QPainter *painter;
    MovingRectangle *testRectangle;
};

#endif // RENDERINGAREA_H
