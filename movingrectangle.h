#ifndef MOVINGRECTANGLE_H
#define MOVINGRECTANGLE_H

#include <QPainter>



class MovingRectangle
{
public:
    MovingRectangle();
    void step();
    void draw(QPainter *painter);

private:
    int increment;
};

#endif // MOVINGRECTANGLE_H
